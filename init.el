(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
   (package-refresh-contents)
   (package-install 'use-package))

(eval-when-compile
   (require 'use-package))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files (quote ("~/org-play/sample-work.org")))
 '(package-selected-packages
   (quote
    (dashboard org-bullets which-key discover-my-major use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(use-package discover-my-major
   :ensure t
   :bind (("C-h C-m" . discover-my-major)))

; https://github.com/justbur/emacs-which-key
(use-package which-key
   :ensure t
   :config
   (progn
      (setq which-key-popup-type 'side-window)
      (setq which-key-compute-remaps t)
;      (which-key-setup-side-window-right)
      (which-key-mode 1)))

(use-package dashboard
      :ensure t
      :diminish dashboard-mode
      :config
      (setq dashboard-items '((recents   . 10)
                                          (bookmarks . 10)))
      (dashboard-setup-startup-hook))

(cua-mode t) ; CUA mode - C-z, C-x, C-c, C-v to undo, cut, copy, paste
(global-visual-line-mode 1)
(setq org-hide-emphasis-markers t)

(setq-default org-catch-invisible-edits 'smart) ; 
(setq org-log-done 'time) ; Add time when tasks moves to DONE state
(show-paren-mode 1)

;; ORG MODE
(setq org-todo-keywords
         (quote ((sequence "TODO(t)" "IN-PROGRESS(i!)" "|" "DONE(d!)")
                     (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "DELEGATED(e@/!)"))))

(setq org-todo-keyword-faces
         (quote (("TODO" :foreground "red" :weight bold)
                     ("IN-PROGRESS" :foreground "blue" :weight bold)
                     ("DONE" :foreground "forest green" :weight bold)
                     ("WAITING" :foreground "orange" :weight bold)
                     ("HOLD" :foreground "magenta" :weight bold)
                     ("CANCELLED" :foreground "forest green" :weight bold)
                     ("DELEGATED" :foreground "forest green" :weight bold))))

(require 'org-mouse)

;; Persist clock-ins across sessions
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

(setq org-default-notes-file "~/capture.org")
(setq org-use-property-inheritance t)

(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key (kbd "<f11>") 'org-clock-in)
(global-set-key (kbd "C-<f11>") 'org-clock-out)
